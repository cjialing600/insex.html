import socket
import pymysql.cursors
from sqlalchemy import Column, String, Integer, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((socket.gethostname(), 1234))
s.listen(5)


class Person(Base):

    __tablename__ = 'testtable'

    id = Column(Integer(), primary_key=True)
    name = Column(String(45))
    sex = Column(String(45))

    def __init__(self, id, name, sex):
        self.id = id
        self.name = name
        self.sex = sex


number = 0
array = ['']*3
i = 0
while i < 3:
    i += 1
    clientsocket, address = s.accept()
    msg = clientsocket.recv(1024)
    #  new = msg.decode("utf-8")   #print(msg.decode("utf-8")) #  print(msg)
    array[number] = msg
    num = number
    number += 1
    while number == 4:
        number = 0
    print(msg.decode("utf-8"))  # print(msg)
    # msg1 =msg.decode("utf-8") #print(msg1)
    while num == 2:
        engine = create_engine(
            'mysql+pymysql://root:King1234@@localhost:3306/source', echo=False)
        DBSession = sessionmaker(bind=engine)
        session = DBSession()
        item1 = Person(id=array[0], name=array[1], sex=array[2])
        session.add(item1)
        session.commit()
        session.close()
        print('data insert database successfully')
        print(f"Connectinon from {address} has been established!")
        clientsocket.send(bytes("Welcome to the server", "utf-8"))
        clientsocket.close()
